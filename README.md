CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------
This module integrates with [Drupal Commerce] to provide a plugable
entity-driven architecture for tax exemption compliance. Drupal Commerce
core and sub-modules provide a lot of ways to calculate tax and set
adjustments upon order processing, but there is currently no standardized way
to provide tax exemption compliance in the ecosystem, until now.

This module ships with a sub-module that provides local storage for tax
exemption documents. The module stores the documents in Drupal's
private:// storage schema.

HOW IT WORKS
------------
This module does not depend on any tax calculation module, it is agnostic of any
3rd party, and simply provides a framework for tax exemption compliance. During
early order processing, applicable tax exemption entities are attached to each
OrderItem through an entity reference field (provided by a CommerceEntityTrait.
In this way, tax calculation modules can apply the exemptions during their own
calculation methods. An example can be found in the [Commerce Tax Exemption
AvaTax Connector] module.

Users can upload and manage their own tax exemptions similar to how they manage
an address book or payment method. When the checkout pane is configured, users
can upload exemption documents during checkout as well.


* For a full description of the module, visit the project page:
  https://www.drupal.org/project/commerce_tax_exemption

* To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/commerce_tax_exemption


REQUIREMENTS
------------

This module requires the following outside of Drupal core:

* Commerce - https://www.drupal.org/project/commerce


INSTALLATION
------------

* As with Drupal Commerce, and its contributed modules, you should install with
  Composer.

```bash
composer require drupal/commerce_tax_exemption:^1.0
```

CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module and its
       dependencies.
    2. Install at least one plugin, you can install the Commerce Tax Exemption
       Local Provider module that ships with this module.
    3. Configure the installed plugin's settings, form modes, and display modes
        by navigating to Administration > Commerce > Configuration >
        Tax Exemptions > Plugins
    4. Enable the plugin's resolver by navigating to Administration > Commerce >
       Configuration > Tax Exemptions > Settings
    5. Configuration permissions for tax exemption entities.
    6. Enable the Tax Exemption trait on your order items by navigating to
        Administration > Commerce > Configuration > Orders > Order item types
    7. Enable the checkout pane in your Checkout Flow if users should be able to
       add exemption entities during checkout.
    8. Add and manage tax exemptions by navigating to Administration > Commerce
        > Tax Exemptions.


MAINTAINERS
-----------

* Alexander Sluiter (alexandersluiter)
  https://www.drupal.org/u/alexandersluiter

[Drupal Commerce]: https://www.drupal.org/project/commerce
[Commerce Tax Exemption AvaTax Connector]: https://drupal.org/project
