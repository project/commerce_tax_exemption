<?php declare(strict_types=1);

namespace Drupal\Tests\commerce_tax_exemption\Kernel;

use Drupal\commerce_tax_exemption\Entity\TaxExemption;
use Drupal\commerce_tax_exemption\TaxExemptionAccessControlHandler;
use Drupal\Tests\commerce\Kernel\CommerceKernelTestBase;

/**
 * @group commerce_tax_exemption
 */
final class TaxExemptionAccessTest extends CommerceKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'file',
    'profile',
    'commerce_tax_exemption',
    'commerce_tax_exemption_local_provider',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    // Populate uid1 to avoid super user permission bugs.
    $this->createUser();
    // Make sure current user context isn't empty.
    $this->container->get('current_user')->setAccount($this->createUser());
  }

  /**
   * Validates access check.
   */
  public function testAccess() {
    $admin = $this->createUser([], ['administer commerce_tax_exemption']);
    $blank_user = $this->createUser();
    $first_user = $this->createUser([], ['manage own commerce_tax_exemption']);
    $second_user = $this->createUser([], ['manage own commerce_tax_exemption']);

    $tax_exemption = TaxExemption::create([
      'provider' => 'local_provider',
      'uid' => $first_user,
    ]);
    $tax_exemption->access('view', $first_user);
    self::assertTrue($tax_exemption->access('view', $first_user));
    self::assertTrue($tax_exemption->access('update', $first_user));
    self::assertTrue($tax_exemption->access('delete', $first_user));

    self::assertTrue($tax_exemption->access('view', $admin));
    self::assertTrue($tax_exemption->access('update', $admin));
    self::assertTrue($tax_exemption->access('delete', $admin));

    self::assertFalse($tax_exemption->access('view', $second_user));
    self::assertFalse($tax_exemption->access('update', $second_user));
    self::assertFalse($tax_exemption->access('delete', $second_user));

    self::assertFalse($tax_exemption->access('view', $blank_user));
    self::assertFalse($tax_exemption->access('update', $blank_user));
    self::assertFalse($tax_exemption->access('delete', $blank_user));
  }

  /**
   * Tests create access checks.
   */
  public function testCreateAccess() {
    $sut = $this->container->get('entity_type.manager')->getAccessControlHandler('commerce_tax_exemption');
    assert($sut instanceof TaxExemptionAccessControlHandler);
    self::assertTrue($sut->createAccess('local_provider', $this->createUser([], ['administer commerce_tax_exemption'])));
    self::assertFalse($sut->createAccess('local_provider', $this->createUser()));
    self::assertTrue($sut->createAccess('local_provider', $this->createUser([], ['manage own commerce_tax_exemption'])));
  }

}
