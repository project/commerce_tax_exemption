<?php declare(strict_types=1);

namespace Drupal\Tests\commerce_tax_exemption\Kernel;

use Drupal\Tests\commerce\Kernel\CommerceKernelTestBase;

/**
 * @group commerce_tax_exemption
 */
final class TaxExemptionEntityTest extends CommerceKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'commerce_tax_exemption',
  ];

  /**
   * Verify the entity definition exists without errors.
   */
  public function testEntityExists() {
    self::assertTrue(
      $this->container->get('entity_type.manager')->hasDefinition('commerce_tax_exemption')
    );
  }

}
