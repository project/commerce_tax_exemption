<?php

namespace Drupal\commerce_tax_exemption;

/**
 * Provides basic plugin configuration information.
 *
 * @package Drupal\commerce_tax_exemption
 */
class TaxExemptionProviderConfig {

  /**
   * The config key for plugins to save their data to.
   */
  public const CONFIGURATION_KEY = "commerce_tax_exemption.plugin.";

  /**
   * The config form form that injects plugin configuration forms.
   */
  public const PLUGIN_BASE_FORM_CLASS = "\Drupal\commerce_tax_exemption\Form\TaxExemptionProviderConfigForm";

  /**
   * The config key for global tax exemption settings.
   */
  public const GLOBAL_DEFAULTS_KEY = "commerce_tax_exemption.settings";

}
