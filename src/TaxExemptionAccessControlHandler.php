<?php

namespace Drupal\commerce_tax_exemption;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the tax exemption entity type.
 */
class TaxExemptionAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    if ($account->hasPermission($this->entityType->getAdminPermission())) {
      return AccessResult::allowed()->cachePerPermissions();
    }

    $result = AccessResult::allowedIf($account->id() == $entity->getOwnerId())
      ->andIf(AccessResult::allowedIfHasPermission($account, 'manage own commerce_tax_exemption'))
      ->addCacheableDependency($entity)
      ->cachePerUser();

    return $result;

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, [
      $this->entityType->getAdminPermission(),
      'manage own commerce_tax_exemption',
    ], 'OR');
  }

}
