<?php

namespace Drupal\commerce_tax_exemption\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines commerce_tax_exemption_provider annotation object.
 *
 * @Annotation
 */
class CommerceTaxExemptionProvider extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * A boolean value to allow approval workflows.
   *
   * Bundle plugins can use the built-in approval workflows if
   * this is set to TRUE.
   *
   * @var bool
   */
  public bool $supports_approval;

  /**
   * A boolean value to allow plugin configuration pages.
   *
   * If the provider supports configuration (api key storage, forms, etc),
   * set this to TRUE and configure the forms.
   *
   * @var bool
   */
  public bool $supports_configuration;

}
