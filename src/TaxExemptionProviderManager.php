<?php

namespace Drupal\commerce_tax_exemption;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * CommerceTaxExemptionProvider plugin manager.
 */
class TaxExemptionProviderManager extends DefaultPluginManager {

  /**
   * Constructs CommerceTaxExemptionProviderPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Commerce/TaxExemptionProvider',
      $namespaces,
      $module_handler,
      'Drupal\commerce_tax_exemption\Plugin\Commerce\TaxExemptionProvider\TaxExemptionProviderInterface',
      'Drupal\commerce_tax_exemption\Annotation\CommerceTaxExemptionProvider'
    );
    $this->alterInfo('commerce_tax_exemption_provider_info');
    $this->setCacheBackend($cache_backend, 'commerce_tax_exemption_provider_plugins');
  }

}
