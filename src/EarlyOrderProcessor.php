<?php

namespace Drupal\commerce_tax_exemption;

use Drupal\commerce_order\OrderProcessorInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Added tax exemption entities to order items, if applicable.
 */
class EarlyOrderProcessor implements OrderProcessorInterface {

  /**
   * Drupal Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The commerce_tax_exemption.tax_exemption_resolver service.
   *
   * @var \Drupal\commerce_tax_exemption\TaxExemptionResolver
   */
  protected $taxExemptionResolver;

  /**
   * Constructs an EarlyOrderProcessor object.
   *
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Drupal Entity Type Manager service.
   * @param \Drupal\commerce_tax_exemption\TaxExemptionResolver $tax_exemption_resolver
   *   The commerce_tax_exemption.tax_exemption_resolver service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, TaxExemptionResolver $tax_exemption_resolver) {
    $this->entityTypeManager = $entity_type_manager;
    $this->taxExemptionResolver = $tax_exemption_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public function process(OrderInterface $order) {
    // Check the order type to make sure tax exemptions are enabled on it.
    // $order_type = $this->entityTypeManager->getStorage('commerce_order_type')->load($order->bundle());
    // @todo Add Store trait? Order trait? How many traits should there be??
    // Should we "enable" tax exemptions on stores? Orders? etc?
    foreach ($order->getItems() as $order_item) {
      // Make sure the order item has the tax exemption trait turned on.
      if ($order_item->hasField('tax_exemptions')) {
        $resolved = $this->taxExemptionResolver->resolve($order_item, TRUE);

        // If any tax exemptions are resolve, add them to the order item.
        if ($resolved) {
          $order_item->set('tax_exemptions', $resolved);
        }
      }
    }
  }

}
