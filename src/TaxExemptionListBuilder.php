<?php

namespace Drupal\commerce_tax_exemption;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for the tax exemption entity type.
 */
class TaxExemptionListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The redirect destination service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * Tax Exemption Provider Plugin Types.
   *
   * @var array
   */
  protected array $taxExemptionTypes;

  /**
   * Drupal CurrentRouteMatch Service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected CurrentRouteMatch $currentRouteMatch;

  /**
   * The user that owns the entities in the list.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * Constructs a new TaxExemptionListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination service.
   * @param \Drupal\commerce_tax_exemption\TaxExemptionProviderManager $taxExemptionProviderManager
   *   TaxExemptionPluginManager service.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   Drupal CurrentRouteMatch Service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, DateFormatterInterface $date_formatter, RedirectDestinationInterface $redirect_destination, TaxExemptionProviderManager $taxExemptionProviderManager, CurrentRouteMatch $current_route_match) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
    $this->redirectDestination = $redirect_destination;
    $this->taxExemptionTypes = $taxExemptionProviderManager->getDefinitions();
    $this->currentRouteMatch = $current_route_match;
    // Set the user.
    $this->user = $this->currentRouteMatch->getParameter('user');
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter'),
      $container->get('redirect.destination'),
      $container->get('plugin.manager.commerce_tax_exemption_provider'),
      $container->get('current_route_match')
    );
  }

  /**
   * Loads entity IDs using a pager sorted by the entity id.
   *
   * @return array
   *   An array of entity IDs.
   */
  protected function getEntityIds() {
    // Check the route to see if this is an admin or user request.
    $query = $this->getStorage()->getQuery();

    $query->accessCheck(TRUE);

    $query->condition('uid', $this->user->id());
    // Sort it.
    $query->sort('id');

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function render() {

    $build['table'] = parent::render();

    $query = $this->getStorage()->getQuery();

    $query->accessCheck(TRUE);

    $query->condition('uid', $this->user->id());

    // Sort it.
    $query->count();

    $total = $query->execute();

    $build['summary']['#markup'] = $this->t('Total tax exemptions: @total', ['@total' => $total]);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['jurisdictions'] = $this->t('Jurisdiction(s)');
    $header['signature_date'] = $this->t('Signed');
    $header['expiration_date'] = $this->t('Expiration');
    $header['expired_status'] = $this->t('State');
    $header['approval'] = $this->t('Approval');
    $header['download'] = '';
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\commerce_tax_exemption\TaxExemptionInterface $entity */
    $row['jurisdictions'] = implode(', ', $entity->getJurisdictionLabels());
    $row['signature_date'] = $this->t('@formatted_date', [
      '@formatted_date' => $entity->getSignatureDate()->format('m-d-Y'),
    ]);
    $row['expiration_date'] = $this->t('@formatted_date', [
      '@formatted_date' => $entity->getExpirationDate()->format('m-d-Y'),
    ]);
    $row['expired_status'] = $this->t('@expired_status', [
      '@expired_status' => $entity->isExpired() ? 'Expired' : 'Available',
    ]);
    $row['approval'] = $this->getApprovalStatus($entity);
    $row['download'] = Link::fromTextAndUrl('Download', $entity->getCertificateUrl())->toString();
    $row = $row + parent::buildRow($entity);
    return $row;
  }

  /**
   * Gets an approval status message for a tax exemption.
   *
   * @param \Drupal\commerce_tax_exemption\TaxExemptionInterface $tax_exemption
   *   The tax exmeption in questions.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Returns a renderable approval status message.
   */
  private function getApprovalStatus(TaxExemptionInterface $tax_exemption) {
    if ($tax_exemption->isApproved() === NULL) {
      return $this->t('Unavailable');
    }

    return $tax_exemption->isApproved() ? $this->t('Approved') : $this->t('Pending');
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    /** @var \Drupal\commerce_tax_exemption\TaxExemption $entity */
    $operations = parent::getDefaultOperations($entity);
    $destination = $this->redirectDestination->getAsArray();
    foreach ($operations as $key => $operation) {
      $operations[$key]['query'] = $destination;
    }

    // Add the approval operations if supported by plugin.
    if ($entity->getProvider()->supportsApproval() && \Drupal::currentUser()->hasPermission('manage approval on any commerce_tax_exemption')) {
      // Set the unapprove operation.
      if ($entity->isApproved()) {
        $operations['unapprove'] = [
          'title' => $this->t('Unapprove'),
          'weight' => 0,
          'url' => $this->ensureDestination(Url::fromRoute('commerce_tax_exemption.unapprove', [
            'commerce_tax_exemption' => $entity->id(),
          ])),
        ];
      }
      else {
        $operations['approve'] = [
          'title' => $this->t('Approve'),
          'weight' => 0,
          'url' => $this->ensureDestination(Url::fromRoute('commerce_tax_exemption.approve', [
            'commerce_tax_exemption' => $entity->id(),
          ])),
        ];
      }
    }

    return $operations;
  }

}
