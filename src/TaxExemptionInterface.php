<?php

namespace Drupal\commerce_tax_exemption;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a tax exemption entity type.
 */
interface TaxExemptionInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the Tax Exemption Provider.
   *
   * @return \Drupal\commerce_tax_exemption\Plugin\Commerce\TaxExemptionProvider\TaxExemptionProviderInterface
   *   A Tax Exemption Plugin Provider instance.
   */
  public function getProvider();

  /**
   * Gets the tax exemption creation timestamp.
   *
   * @return int
   *   Creation timestamp of the tax exemption.
   */
  public function getCreatedTime();

  /**
   * Sets the tax exemption creation timestamp.
   *
   * @param int $timestamp
   *   The tax exemption creation timestamp.
   *
   * @return \Drupal\commerce_tax_exemption\TaxExemptionInterface
   *   The called tax exemption entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the tax exemption status.
   *
   * @return bool
   *   TRUE if the tax exemption is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the tax exemption status.
   *
   * @param bool $status
   *   TRUE to enable this tax exemption, FALSE to disable.
   *
   * @return \Drupal\commerce_tax_exemption\TaxExemptionInterface
   *   The called tax exemption entity.
   */
  public function setStatus($status);

  /**
   * Gets the date a tax exemption was signed.
   *
   * @return \Drupal\Core\TypedData\Type\DateTimeInterface|null
   *   DateTimeInterface definition.
   */
  public function getSignatureDate();

  /**
   * Gets the expiration date for a tax exemption.
   *
   * @return \Drupal\Core\TypedData\Type\DateTimeInterface|null
   *   DateTimeInterface definition.
   */
  public function getExpirationDate();

  /**
   * Gets a URL to download / view the exemption certificate.
   *
   * @return \Drupal\Core\Url
   *   Renderable URL object.
   */
  public function getCertificateUrl();

  /**
   * Gets the Exemption Reason for display.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Renderable text string.
   */
  public function getReason();

  /**
   * Gets the validity of a TaxExemption entity.
   *
   * @param bool $require_approval
   *   If provider supports approval, this should be handled in the plugin.
   *
   * @return bool
   *   TRUE if valid, otherwise FALSE.
   */
  public function isValid(bool $require_approval);

  /**
   * Gets the approval status of the tax exemption, if supported.
   *
   * @return bool|null
   *   TRUE if approved, FALSE if not approved.
   *   NULL if not supported by provider.
   */
  public function isApproved();

  /**
   * Gets the Expiration status of the Exemption entity.
   *
   * @return bool
   *   Returns TRUE if expired, FALSE otherwise.
   */
  public function isExpired();

  /**
   * Approves this tax exemption, if supported by provider plugin.
   *
   * @return bool|null
   *   Returns TRUE if success, FALSE otherwise.
   *   NULL if not supported by provider.
   */
  public function setAsApproved();

  /**
   * Unapproves this tax exemption, if supported by provider plugin.
   *
   * @return bool|null
   *   Returns TRUE if success, FALSE otherwise.
   *   NULL if not supported by provider.
   */
  public function setAsUnapproved();

  /**
   * Gets the label for the jurisdictions an exemption is valid for.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]
   *   Renderable text string.
   */
  public function getJurisdictionLabels();

  /**
   * Returns the taxable types the exemption can apply to.
   *
   * The array key MUST be the taxable type.
   *
   * @return array
   *   The key => value array.
   *
   * @example
   *   Example:
   *   [
   *   'physical_goods' => 'physical_goods',
   *   'digital_goods' => 'digital_goods'
   *   ]
   */
  public function getTaxableTypes();

  /**
   * Checks whether a TaxExemption entity is valid for a given order item.
   *
   * This is usually done by comparing the taxable types
   * on the order item bundle to the taxable types on the exemption entity.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   *   The order item to check against.
   *
   * @return bool
   *   TRUE if the TaxExemption entity applies to the order item.
   */
  public function applies(OrderItemInterface $order_item);

  /**
   * Determines whether the entity can be deleted.
   *
   * Considering that this is a compliance module, tax exemption
   * entities should not be able to be deleted it they are attached
   * to any order item.
   *
   * @return bool
   *   Returns TRUE if deletion is allowed, otherwise FALSE.
   */
  public function deletionAllowed();

  /**
   * Gets the order item entities the tax exemption is attached to.
   *
   * @return \Drupal\commerce_order\Entity\OrderItem[]
   *   Returns an array or OrderItem entities.
   */
  public function getOrderItems();

  /**
   * Gets the order item ids that include this tax exemption.
   *
   * @return array
   *   Returns an query result array of order item ids.
   */
  public function getOrderItemIds();

  /**
   * Gets the order ids that include this tax exemption.
   *
   * @return array
   *   Returns a unique array of order ids.
   */
  public function getOrderIds();

  /**
   * Gets the order entities this tax exemption is attached to.
   *
   * @return \Drupal\commerce_order\Entity\Order[]
   *   Returns the Order entities.
   */
  public function getOrders();

}
