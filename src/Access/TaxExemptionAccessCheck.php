<?php

namespace Drupal\commerce_tax_exemption\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserInterface;

/**
 * Checks access for payment method routes.
 *
 * @see \Drupal\Core\Access\CustomAccessCheck
 */
class TaxExemptionAccessCheck {

  /**
   * Checks access.
   *
   * Confirms that the user either has the 'administer commerce_tax_exemption'
   * permission, or the 'manage own commerce_tax_exemption' permission while
   * visiting their own tax exemption pages.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function checkAccess(RouteMatchInterface $route_match, AccountInterface $account) {

    $result = AccessResult::allowedIfHasPermissions($account, [
      'administer tax exemption',
    ]);

    $route_user = $route_match->getParameter('user');
    if ($result->isNeutral() && $route_user instanceof UserInterface && $route_user->id() == $account->id()) {
      $result = AccessResult::allowedIfHasPermissions($account, [
        'manage own commerce_tax_exemption',
      ])->cachePerUser();
    }

    return $result;
  }

}
