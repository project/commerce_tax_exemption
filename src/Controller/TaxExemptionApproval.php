<?php

namespace Drupal\commerce_tax_exemption\Controller;

use Drupal\commerce_tax_exemption\TaxExemptionInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Routing\RedirectDestination;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns responses for Tax Exemptions routes.
 */
class TaxExemptionApproval extends ControllerBase {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $userAccount;

  /**
   * Drupal Current Route Match Service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * Drupal Redirect Destination Service.
   *
   * @var \Drupal\Core\Routing\RedirectDestination
   */
  protected $redirectDestination;

  /**
   * Permission required to manage approvals.
   *
   * @var string
   */
  protected $approvalPermission = 'manage approval on any commerce_tax_exemption';

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Session\AccountInterface $user_account
   *   Drupal CurrentUser service.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   Drupal CurrentRouteMatch Service.
   * @param \Drupal\Core\Routing\RedirectDestination $redirect_destination
   *   Drupal redirect destination service.
   */
  public function __construct(MessengerInterface $messenger, AccountInterface $user_account, CurrentRouteMatch $current_route_match, RedirectDestination $redirect_destination) {
    $this->messenger = $messenger;
    $this->userAccount = $user_account;
    $this->currentRouteMatch = $current_route_match;
    $this->redirectDestination = $redirect_destination;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('current_user'),
      $container->get('current_route_match'),
      $container->get('redirect.destination')
    );
  }

  /**
   * Sets the tax exemption entity as approved.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Symfony RedirectResponse object.
   */
  public function approve() {
    // Check for permissions first.
    if (!$this->userAccount->hasPermission($this->approvalPermission)) {
      throw new AccessDeniedException();
    }

    // Make sure the route has a tax exemption entity match.
    if (!$this->currentRouteMatch->getParameter('commerce_tax_exemption') instanceof TaxExemptionInterface) {
      throw new NotFoundHttpException();
    }

    /** @var \Drupal\commerce_tax_exemption\TaxExemptionInterface $tax_exemption */
    $tax_exemption = $this->currentRouteMatch->getParameter('commerce_tax_exemption');

    // If the exemption is already approved, do nothing to it and warn the user.
    if (!$tax_exemption->isApproved()) {
      // Send the approval signal the the exemption entity.
      $approval_response = $tax_exemption->setAsApproved();

      // Depending on response, throw a message at the user.
      if ($approval_response) {
        $this->messenger()->addMessage($this->t('Tax Exemption @label approved.', [
          '@label' => $tax_exemption->label(),
        ]));
      }
      else {
        $this->messenger()->addError($this->t('The tax exemption failed to approve properly'));
      }
    }
    else {
      $this->messenger()
        ->addError($this->t('Tax Exemption @label is already approved', [
          '@label' => $tax_exemption->label(),
        ]));
    }

    return $this->redirectUser();
  }

  /**
   * Sets the tax exemption entity as unapproved.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Symfony RedirectResponse object.
   */
  public function unapprove() {
    // Check for permissions first.
    if (!$this->userAccount->hasPermission($this->approvalPermission)) {
      throw new AccessDeniedException();
    }

    // Make sure the route has a tax exemption entity match.
    if (!$this->currentRouteMatch->getParameter('commerce_tax_exemption') instanceof TaxExemptionInterface) {
      throw new NotFoundHttpException();
    }

    /** @var \Drupal\commerce_tax_exemption\TaxExemptionInterface $tax_exemption */
    $tax_exemption = $this->currentRouteMatch->getParameter('commerce_tax_exemption');

    // If the exemption is already approved, do nothing to it and warn the user.
    if ($tax_exemption->isApproved()) {
      // Send the unapproval signal the the exemption entity.
      $approval_response = $tax_exemption->setAsUnapproved();

      // Depending on response, throw a message at the user.
      if ($approval_response) {
        $this->messenger()->addMessage($this->t('Tax Exemption @label disapproved.', [
          '@label' => $tax_exemption->label(),
        ]));
      }
      else {
        $this->messenger()->addError($this->t('The tax exemption failed to disapprove properly'));
      }
    }
    else {
      $this->messenger()
        ->addError($this->t('Tax Exemption @label is already disapproved', [
          '@label' => $tax_exemption->label(),
        ]));
    }

    return $this->redirectUser();
  }

  /**
   * Redirects the user to their destination if available.
   *
   * Falls back to the tax exemption admin page.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Symfony RedirectResponse object.
   */
  private function redirectUser() {
    if ($destination = $this->redirectDestination->getAsArray()['destination']) {
      return new RedirectResponse($this->redirectDestination->getAsArray()['destination']);
    }
    else {
      return new RedirectResponse(Url::fromRoute('view.administer_tax_exemptions.page_1'));
    }
  }

}
