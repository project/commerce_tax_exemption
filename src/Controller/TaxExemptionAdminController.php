<?php

namespace Drupal\commerce_tax_exemption\Controller;

use Drupal\commerce_tax_exemption\TaxExemptionProviderConfig;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

/**
 * Controller for admin routes for entity types using non-entity bundles.
 */
class TaxExemptionAdminController {

  use StringTranslationTrait;

  /**
   * Callback for the admin overview route.
   */
  public function getAdminPage(RouteMatchInterface $route_match) {

    $entity_type_id = $route_match->getRouteObject()->getDefault('entity_type_id');
    $entity_type = \Drupal::service('entity_type.manager')->getDefinition($entity_type_id);
    $entity_bundle_info = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type_id);

    $build = [];

    $build['table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Name'),
        $this->t('Description'),
        $this->t('Operations'),
      ],
      '#rows' => [],
      '#empty' => $this->t('There are no @label yet.', [
        '@label' => $entity_type->getPluralLabel(),
      ]),
    ];

    foreach ($entity_bundle_info as $bundle_name => $bundle_info) {
      $build['table']['#rows'][$bundle_name] = [
        'name' => ['data' => $bundle_info['label']],
        'description' => ['data' => $bundle_info['description'] ?? ''],
        'operations' => ['data' => $this->buildOperations($entity_type_id, $bundle_name)],
      ];
    }

    return $build;
  }

  /**
   * Doubles as a Field UI dummy route and a plugin config page.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route.
   * @param string|null $bundle
   *   The bundle plugin to configure.
   *
   * @return array
   *   A form render array to configure the bundle plugin.
   */
  public function getPluginConfigurationPage(RouteMatchInterface $route_match, $bundle = NULL) {

    // Add PluginForm here.
    if ($bundle) {
      // Add the plugin form here.
      return \Drupal::formBuilder()->getForm(TaxExemptionProviderConfig::PLUGIN_BASE_FORM_CLASS);
    }
    else {
      return [
        '#markup' => $this->t('There was a problem building the bundle plugin form.'),
      ];
    }
  }

  /**
   * Builds a renderable list of operation links for the bundle.
   *
   * @param string $entity_type_id
   *   The entity type id to build operations for.
   * @param string $bundle_name
   *   The bundle name to build operations for.
   *
   * @return array
   *   A renderable array of operation links.
   *
   * @see \Drupal\Core\Entity\EntityListBuilder::buildRow()
   */
  public function buildOperations($entity_type_id, $bundle_name) {
    $operations = [];

    $entity_type = \Drupal::entityTypeManager()->getDefinition($entity_type_id);

    $admin_permission = $entity_type->getAdminPermission();

    // Copy the work of field_ui_entity_operation(), which we can't use because
    // it expects an entity.
    $current_user = \Drupal::currentUser();
    if ($current_user->hasPermission($admin_permission)) {
      $operations['edit'] = [
        'title' => $this->t('Configure Plugin'),
        'weight' => 0,
        'url' => Url::fromRoute("entity.{$entity_type_id}.plugin_configuration", [
          'bundle' => $bundle_name,
        ]),
      ];

      $operations['manage-fields'] = [
        'title' => $this->t('Manage fields'),
        'weight' => 15,
        'url' => Url::fromRoute("entity.{$entity_type_id}.field_ui_fields", [
          'bundle' => $bundle_name,
        ]),
      ];
      $operations['manage-form-display'] = [
        'title' => $this->t('Manage form display'),
        'weight' => 20,
        'url' => Url::fromRoute("entity.entity_form_display.{$entity_type_id}.default", [
          'bundle' => $bundle_name,
        ]),
      ];
      $operations['manage-display'] = [
        'title' => $this->t('Manage display'),
        'weight' => 25,
        'url' => Url::fromRoute("entity.entity_view_display.{$entity_type_id}.default", [
          'bundle' => $bundle_name,
        ]),
      ];
    }

    return [
      '#type' => 'operations',
      '#links' => $operations,
    ];
  }

}
