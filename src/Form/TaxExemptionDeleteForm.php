<?php

namespace Drupal\commerce_tax_exemption\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Extends ContentEntityDeleteForm to add constraint functionality.
 *
 * @package Drupal\commerce_tax_exemption\Form
 */
class TaxExemptionDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   *
   * Adds deletion constraints and disables the form if entity cannot
   * be deleted for any reason.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    /** @var \Drupal\commerce_tax_exemption\Entity\TaxExemption $tax_exemption */
    $tax_exemption = $this->entity;

    // Verify that this entity is not being used in any orders.
    if (!$tax_exemption->deletionAllowed()) {
      $order_item_count = count($tax_exemption->getOrderItemIds());
      $order_count = count($tax_exemption->getOrderIds());

      // Disable the submit (delete) button.
      $form['actions']['submit']['#attributes']['disabled'] = 'disabled';

      // Modify the form description element to state the non-deletion status.
      $form['description'] = [
        '#markup' => $this->t('This tax exemption is currently attached to @order_item_count @order_item_label in @order_count @order_label and cannot be deleted.', [
          '@order_item_count' => $order_item_count,
          '@order_item_label' => $this->getEntityLabel('commerce_order_item', $order_item_count),
          '@order_count' => $order_count,
          '@order_label' => $this->getEntityLabel('commerce_order', $order_count),
        ]),
      ];
    }

    return $form;
  }

  /**
   * Gets the proper entity label for a given entity type.
   *
   * @param string $entity_type_id
   *   The entity type id to load.
   * @param int $count
   *   The number of entities in question.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|string
   *   Returns the plural label if there are more than one entities or not zero.
   */
  private function getEntityLabel(string $entity_type_id, int $count) {
    if ($count > 1 && $count !== 0) {
      return $this->entityTypeManager->getDefinition($entity_type_id)->getPluralLabel();
    }
    else {
      return $this->entityTypeManager->getDefinition($entity_type_id)->getSingularLabel();
    }
  }

}
