<?php

namespace Drupal\commerce_tax_exemption\Form;

use Drupal\commerce_tax_exemption\TaxExemptionProviderConfig;
use Drupal\commerce_tax_exemption\TaxExemptionProviderManager;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Tax Exemptions settings for this site.
 */
class TaxExemptionProviderConfigForm extends FormBase {

  /**
   * Tax Exemption Provider Plugin Manger.
   *
   * @var \Drupal\commerce_tax_exemption\TaxExemptionProviderManager
   */
  protected $pluginManager;

  /**
   * The instance of the Tax Exemption Plugin.
   *
   * @var \Drupal\commerce_tax_exemption\Plugin\Commerce\TaxExemptionProvider\TaxExemptionProviderInterface
   */
  protected $bundlePlugin;

  /**
   * Constructs a new Tax Exemption Provider plugin form.
   *
   * @param \Drupal\commerce_tax_exemption\TaxExemptionProviderManager $plugin_manager
   *   Tax Exemption Provider plugin manager.
   * @param Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   Drupal's CurrentRouteMatch Service.
   */
  public function __construct(TaxExemptionProviderManager $plugin_manager, CurrentRouteMatch $current_route_match) {
    $this->pluginManager = $plugin_manager;
    $this->bundlePlugin = $this->pluginManager->createInstance($current_route_match->getParameter('bundle'));
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.commerce_tax_exemption_provider'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_tax_exemption_plugin';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      TaxExemptionProviderConfig::CONFIGURATION_KEY,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get the plugin form.
    return $this->bundlePlugin->buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate the plugin form.
    $this->bundlePlugin->validateConfigurationForm($form, $form_state);

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Submit the plugin's form.
    return $this->bundlePlugin->submitConfigurationForm($form, $form_state);
  }

}
