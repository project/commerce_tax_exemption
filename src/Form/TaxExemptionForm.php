<?php

namespace Drupal\commerce_tax_exemption\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the tax exemption entity edit forms.
 */
class TaxExemptionForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   *
   * Added renderer service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, protected RendererInterface $renderer) {
    $this->entityRepository = $entity_repository;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('renderer'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form_display = $this->getFormDisplay($form_state);

    // Pass the plugin and form mode to the form for theme hooks.
    $form['#bundle_plugin'] = $form_display->getTargetBundle();
    $form['#form_mode'] = $form_display->getMode();

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * When adding a new TaxExemption entity, use the default bundle plugin.
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    if ($route_match->getRawParameter('commerce_tax_exemption') !== NULL) {
      $entity = $route_match->getParameter('commerce_tax_exemption');
    }
    else {
      $values = [
        'provider' => $route_match->getParameter('provider'),
        'uid' => $route_match->getParameter('user'),
      ];
      $entity = $this->entityTypeManager->getStorage('commerce_tax_exemption')->create($values);
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => $this->renderer->render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New tax exemption %label has been created.', $message_arguments));
      $this->logger('commerce_tax_exemption')->notice('Created new tax exemption %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The tax exemption %label has been updated.', $message_arguments));
      $this->logger('commerce_tax_exemption')->notice('Updated new tax exemption %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.commerce_tax_exemption.canonical', [
      'commerce_tax_exemption' => $entity->id(),
      'user' => $entity->getOwner()->id(),
    ]);
  }

}
