<?php

namespace Drupal\commerce_tax_exemption\Form;

use Drupal\commerce_tax_exemption\TaxExemptionProviderManager;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form for a tax exemption entity type.
 */
class TaxExemptionSettingsForm extends ConfigFormBase {

  /**
   * Tax Exemption Plugin Provider Manager.
   *
   * @var \Drupal\commerce_tax_exemption\TaxExemptionProviderManager
   */
  protected $taxExemptionProviderManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, TaxExemptionProviderManager $tax_exemption_provider_manager) {
    parent::__construct($config_factory);

    $this->taxExemptionProviderManager = $tax_exemption_provider_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.commerce_tax_exemption_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_tax_exemption_settings';
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return [
      'commerce_tax_exemption.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('commerce_tax_exemption.settings');

    $form['settings'] = [
      '#markup' => $this->t('Settings form for a tax exemption entity type.'),
    ];

    $installed_plugin_options = $this->getInstalledPluginOptions();

    $form['enabled_plugin_resolvers'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled Plugin Resolvers'),
      '#description' => $this->t('During checkout or order processing, which exemption plugins should be allowed to resolve?'),
      '#default_value' => $config->get('enabled_plugin_resolvers') ? $config->get('enabled_plugin_resolvers') : [],
      '#options' => $installed_plugin_options,
    ];

    $form['default_provider_plugin'] = [
      '#type' => 'select',
      '#title' => $this->t('Customer Default Bundle'),
      '#description' => $this->t('When customers are creating tax exemptions, which plugin should be used?'),
      '#options' => $installed_plugin_options,
      '#default_value' => $config->get('default_provider_plugin'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('commerce_tax_exemption.settings')
      ->set('enabled_plugin_resolvers', $form_state->getValue('enabled_plugin_resolvers'))
      ->set('default_provider_plugin', $form_state->getValue('default_provider_plugin'))
      ->save();

    $this->messenger()->addStatus($this->t('The configuration has been updated.'));
  }

  /**
   * Gets an options list of installed plugins.
   *
   * @return array
   *   Options list of installed plugins.
   */
  private function getInstalledPluginOptions() {
    $options = [];

    $definitions = $this->taxExemptionProviderManager->getDefinitions();

    foreach ($definitions as $definition) {
      $options[$definition['id']] = $definition['label'];
    }

    return $options;
  }

}
