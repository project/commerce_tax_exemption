<?php

namespace Drupal\commerce_tax_exemption\Entity;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_order\Entity\OrderItemType;
use Drupal\commerce_tax\TaxableType;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\commerce_tax_exemption\TaxExemptionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the tax exemption entity class.
 *
 * @ContentEntityType(
 *   id = "commerce_tax_exemption",
 *   label = @Translation("Tax Exemption"),
 *   label_collection = @Translation("Tax Exemptions"),
 *   label_singular = @Translation("tax exemption"),
 *   label_plural = @Translation("tax exemptions"),
 *   label_count = @PluralTranslation(
 *     singular = "@count tax exemption",
 *     plural = "@count tax exemptions",
 *   ),
 *   bundle_label = @Translation("Tax Exemption Provider"),
 *   bundle_plugin_type = "commerce_tax_exemption_provider",
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\commerce_tax_exemption\TaxExemptionListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\commerce_tax_exemption\TaxExemptionAccessControlHandler",
 *     "storage" = "Drupal\commerce_tax_exemption\TaxExemptionStorage",
 *     "storage_schema" = "Drupal\commerce\CommerceContentEntityStorageSchema",
 *     "views_data" = "Drupal\commerce\CommerceEntityViewsData",
 *     "form" = {
 *       "user" = "Drupal\commerce_tax_exemption\Form\TaxExemptionForm",
 *       "add" = "Drupal\commerce_tax_exemption\Form\TaxExemptionForm",
 *       "edit" = "Drupal\commerce_tax_exemption\Form\TaxExemptionForm",
 *       "delete" = "Drupal\commerce_tax_exemption\Form\TaxExemptionDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\commerce_tax_exemption\TaxExemptionRouteProvider",
 *     }
 *   },
 *   base_table = "commerce_tax_exemption",
 *   revision_table = "commerce_tax_exemption_revision",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer commerce_tax_exemption",
 *   permission_granularity = "entity_type",
 *   field_indexes = {},
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *     "bundle" = "provider",
 *     "owner" = "uid",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "add-page" = "/admin/commerce/tax-exemptions/add",
 *     "add-form" = "/admin/commerce/tax-exemptions/add/{provider}",
 *     "edit-form" = "/admin/commerce/tax-exemptions/edit/{commerce_tax_exemption}",
 *     "user-add-page" = "/user/{user}/tax-exemptions/add",
 *     "user-add-form" = "/user/{user}/tax-exemptions/add/{provider}",
 *     "user-edit-form" = "/user/{user}/tax-exemptions/{commerce_tax_exemption}/edit",
 *     "delete-form" = "/user/{user}/tax-exemptions/{commerce_tax_exemption}/delete",
 *     "collection" = "/user/{user}/tax-exemptions",
 *     "canonical" = "/user/{user}/tax-exemptions/{commerce_tax_exemption}",
 *     "plugin-configuration" = "/admin/commerce/config/tax-exemptions/plugins",
 *   },
 *   field_ui_base_route = "entity.commerce_tax_exemption.plugin_configuration"
 * )
 */
class TaxExemption extends RevisionableContentEntityBase implements TaxExemptionInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getProvider() {
    /** @var \Drupal\commerce_tax_exemption\TaxExemptionProviderManager $provider_plugin_manager */
    $provider_plugin_manager = \Drupal::service('plugin.manager.commerce_tax_exemption_provider');
    return $provider_plugin_manager->createInstance($this->bundle());
  }

  /**
   * {@inheritdoc}
   *
   * We need to add the owner to the route parameters.
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);
    $uri_route_parameters['user'] = $this->getOwnerId();
    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   *
   * Do not allow deletion if deletionAllowed() comes back negative.
   */
  public function delete() {
    if ($this->deletionAllowed()) {
      parent::delete();
    }
    else {
      \Drupal::logger('commerce_tax_exemption')->error('Cannot delete tax exemption entity id ' . $this->id() . ' as it is currently attached to order items. You must first delete the order items if you would like to delete this tax exemption.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSignatureDate() {
    return $this->getProvider()->getSignatureDate($this);
  }

  /**
   * {@inheritdoc}
   */
  public function getExpirationDate() {
    return $this->getProvider()->getExpirationDate($this);
  }

  /**
   * {@inheritdoc}
   */
  public function getCertificateUrl() {
    return $this->getProvider()->getCertificateUrl($this);
  }

  /**
   * {@inheritdoc}
   */
  public function getReason() {
    return $this->getProvider()->getReason($this);
  }

  /**
   * {@inheritdoc}
   */
  public function isValid($require_approval = FALSE) {
    return $this->getProvider()->isValid($this, $require_approval);
  }

  /**
   * {@inheritdoc}
   */
  public function isApproved() {
    if ($this->getProvider()->supportsApproval()) {
      return $this->getProvider()->isApproved($this);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setAsApproved() {
    return $this->getProvider()->setAsApproved($this);
  }

  /**
   * {@inheritdoc}
   */
  public function setAsUnapproved() {
    return $this->getProvider()->setAsUnapproved($this);
  }

  /**
   * {@inheritdoc}
   */
  public function getJurisdictionLabels() {
    return $this->getProvider()->getJurisdictionLabels($this);
  }

  /**
   * {@inheritdoc}
   */
  public function isExpired() {
    if ($this->getExpirationDate()->getPhpDateTime()->getTimeStamp() < time()) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function applies(OrderItemInterface $order_item) {
    // Get the order item type so we can make sure it's taxable.
    /** @var \Drupal\commerce_order\Entity\OrderItemType $bundle */
    $bundle = \Drupal::entityTypeManager()->getStorage('commerce_order_item_type')->load($order_item->bundle());
    // If the order item type has tax enabled and configured, we can proceed.
    if ($bundle->getThirdPartySettings('commerce_tax') && $taxable_type = $bundle->getThirdPartySetting('commerce_tax', 'taxable_type')) {
      // The order item taxable type should match at least one on this entity.
      if (array_key_exists($taxable_type, $this->getTaxableTypes())) {
        return TRUE;
      }
    }

    // Default to FALSE.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function deletionAllowed() {
    // First check if tax exemptions are enabled on any order item types.
    $has_trait = [];
    foreach (OrderItemType::loadMultiple() as $order_item_type) {
      if ($order_item_type->hasTrait('order_item_tax_exemptions')) {
        $has_trait[] = $order_item_type->id();
      }
    }

    if ($has_trait && $this->getOrderItemIds()) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderItemIds() {
    $query = $this->entityTypeManager()->getStorage('commerce_order_item')->getQuery();
    $query->accessCheck(FALSE);

    $query->condition('tax_exemptions', $this->id(), 'IN');

    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderItems() {
    return OrderItem::loadMultiple($this->getOrderItemIds());
  }

  /**
   * {@inheritdoc}
   */
  public function getOrders() {
    return Order::loadMultiple($this->getOrderIds());
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderIds() {
    $order_ids = [];

    $order_items = $this->getOrderItems();
    foreach ($order_items as $order_item) {
      $id = $order_item->getOrderId();
      $order_ids[$id] = $id;
    }

    return $order_ids;
  }

  /**
   * {@inheritdoc}
   */
  public function getTaxableTypes() {
    $types = [];

    if (!$this->get('taxable_types')->isEmpty()) {
      foreach ($this->get('taxable_types') as $list_string_item) {
        $value = $list_string_item->getString();
        $types[$value] = $value;
      }
    }

    return $types;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += self::ownerBaseFieldDefinitions($entity_type);

    $fields['tax_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Tax ID'))
      ->setDescription(t('The Tax ID on the certificate.'))
      ->setRevisionable(TRUE)
      ->setRequired(TRUE)
      ->setCardinality(1)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setRevisionable(TRUE)
      ->setLabel(t('Status'))
      ->setDescription(t('A boolean indicating whether the tax exemption is enabled.'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid']
      ->setLabel(t('Owner'))
      ->setDescription(t('The user ID of the tax exemption author.'))
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the tax exemption was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the tax exemption was last edited.'));

    $fields['taxable_types'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Taxable Type(s)'))
      ->setDescription(t('The taxable good types that this exemption covers.'))
      ->setRevisionable(TRUE)
      ->setRequired(TRUE)
      ->setCardinality(-1)
      ->setDefaultValue(TaxableType::getDefault())
      ->setSettings(
        [
          'allowed_values' => TaxableType::getLabels(),
        ]
      )
      ->setDisplayOptions('form', [
        'type' => 'checkboxes',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
