<?php

namespace Drupal\commerce_tax_exemption;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\profile\Entity\Profile;

/**
 * Service to resolve TaxExemption entities for orders.
 */
class TaxExemptionResolver {

  /**
   * Default Configuration Key.
   *
   * @var string
   */
  protected $configurationKey = 'commerce_tax_exemption.settings';

  /**
   * The plugin.manager.commerce_tax_exemption_provider service.
   *
   * @var \Drupal\commerce_tax_exemption\TaxExemptionProviderManager
   */
  protected $taxExemptionPluginManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Configuration object for getting module defaults.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a TaxExemptionResolver object.
   *
   * @param \Drupal\commerce_tax_exemption\TaxExemptionProviderManager $plugin_manager_commerce_tax_exemption_provider
   *   The plugin.manager.commerce_tax_exemption_provider service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(TaxExemptionProviderManager $plugin_manager_commerce_tax_exemption_provider, ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->taxExemptionPluginManager = $plugin_manager_commerce_tax_exemption_provider;
    $this->entityTypeManager = $entity_type_manager;
    $this->config = $config_factory->get('commerce_tax_exemption.settings');
  }

  /**
   * Resolves Tax Exemptions for all enabled plugins.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   *   The order to resolve exemptions for.
   * @param bool $require_approval
   *   Used only if provider plugin supports approval.
   *
   * @see \Drupal\commerce_tax_exemption\Annotation\CommerceTaxExemptionProvider
   *
   * @return array|\Drupal\commerce_tax_exemption\Entity\TaxExemption[]
   *   An array of TaxExemption entities to apply to an order.
   */
  public function resolve(OrderItemInterface $order_item, bool $require_approval = TRUE) {
    // Make sure the order item has the tax exemption trait turned on.
    if (!$this->getOrderItemBundle($order_item)->hasTrait('order_item_tax_exemptions')) {
      return NULL;
    }

    // Make sure there are plugins enabled.
    $enabled_plugin_resolvers = $this->config->get('enabled_plugin_resolvers');
    if (!$enabled_plugin_resolvers) {
      return NULL;
    }
    $exemptions = [];

    // Get the taxable profile for this order item.
    $profile = $this->getTaxableProfile($order_item->getOrder());

    // If there is no taxable profile yet, fail out.
    if (!$profile) {
      return NULL;
    }

    foreach ($enabled_plugin_resolvers as $enabled_plugin_resolver) {
      /** @var \Drupal\commerce_tax_exemption\Plugin\Commerce\TaxExemptionProvider\TaxExemptionProviderInterface $instance */
      $instance = $this->taxExemptionPluginManager->createInstance($enabled_plugin_resolver);

      $resolved = $instance->resolve($order_item, $profile, $require_approval);
      // Add each resolved exemption to the return array.
      foreach ($resolved as $exemption) {
        $exemptions[$exemption->id()] = $exemption;
      }
    }

    return $exemptions;
  }

  /**
   * Gets the Order Item Type entity.
   *
   * @param \Drupal\commerce_order\Entity\OrderItem $order_item
   *   The order item to get the bundle for.
   *
   * @return \Drupal\commerce_order\Entity\OrderItemType
   *   Returns the bundle type entity.
   */
  private function getOrderItemBundle(OrderItem $order_item) {
    /** @var \Drupal\commerce_order\Entity\OrderItemType $bundle */
    $bundle = $this->entityTypeManager->getStorage('commerce_order_item_type')->load($order_item->bundle());
    return $bundle;
  }

  /**
   * Gets the taxable profiles for the order.
   *
   * If Commerce Shipping is installed, this should return the
   * shipment profiles first and fall back to the billing profile.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order to get taxable profiles for.
   *
   * @return \Drupal\profile\Entity\Profile[]|null
   *   Returns an array of taxable profiles applicable to the order.
   */
  public function getTaxableProfile(OrderInterface $order) {
    $order_profiles = $order->collectProfiles();

    if (isset($order_profiles['shipping'])) {
      return $this->getOriginalProfile($order_profiles['shipping']);
    }
    elseif (isset($order_profiles['billing'])) {
      return $this->getOriginalProfile($order_profiles['billing']);
    }

    return NULL;
  }

  /**
   * Commerce copies profiles during checkout.
   *
   * This gets the originals needed for tax exemption jurisdictions.
   *
   * @param \Drupal\profile\Entity\Profile $profile
   *   The profiles attached to the order that are copies of originals.
   *
   * @return \Drupal\profile\Entity\Profile|null
   *   The original profile that was copied by Commerce Checkout.
   *
   * @see \Drupal\commerce_order\AddressBook::copy()
   */
  private function getOriginalProfile(Profile $profile) {
    if ($original_profile_id = $profile->getData('address_book_profile_id')) {
      return Profile::load($original_profile_id);
    }

    return NULL;
  }

}
