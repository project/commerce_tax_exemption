<?php

namespace Drupal\commerce_tax_exemption\Plugin\Commerce\TaxExemptionProvider;

use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_tax_exemption\Entity\TaxExemption;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\entity\BundlePlugin\BundlePluginInterface;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Interface for commerce_tax_exemption_provider plugins.
 */
interface TaxExemptionProviderInterface extends BundlePluginInterface, ConfigurableInterface, PluginFormInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * Gets the plugin definition setting.
   *
   * @return bool
   *   TRUE if provider supports approval, otherwise omit or set to FALSE.
   */
  public function supportsApproval();

  /**
   * Gets the plugin definition setting for configuration.
   *
   * @return bool
   *   TRUE if provider supports configuration.
   */
  public function supportsConfiguration();

  /**
   * When an exemption entity is saved, this will be called on the plugin.
   *
   * @param \Drupal\commerce_tax_exemption\Entity\TaxExemption $tax_exemption
   *   The entity about to be saved.
   *
   * @return \Drupal\commerce_tax_exemption\Entity\TaxExemption
   *   The entity to finish saving.
   */
  public function save(TaxExemption $tax_exemption);

  /**
   * Used if the exemption provider provides support to approval.
   *
   * This will run when isApproved() is called from the Tax Exemption entity.
   *
   * @param \Drupal\commerce_tax_exemption\Entity\TaxExemption $tax_exemption
   *   The tax exemption in question.
   *
   * @return bool|null
   *   TRUE if approved, FALSE otherwise, NULL if unsupported.
   */
  public function isApproved(TaxExemption $tax_exemption);

  /**
   * Sets the entity as approved, if supported by plugin.
   *
   * @param \Drupal\commerce_tax_exemption\Entity\TaxExemption $exemption
   *   The Tax Exemption entity.
   *
   * @return bool|null
   *   TRUE if success, FALSE if failed, NULL if unsupported by provider.
   */
  public function setAsApproved(TaxExemption $exemption);

  /**
   * Sets the entity as unapproved, if supported by plugin.
   *
   * @param \Drupal\commerce_tax_exemption\Entity\TaxExemption $tax_exemption
   *   The Tax Exemption entity.
   *
   * @return bool|null
   *   TRUE if success, FALSE if failed, NULL if unsupported by provider.
   */
  public function setAsUnapproved(TaxExemption $tax_exemption);

  /**
   * Gets the date a tax exemption was signed.
   *
   * @param \Drupal\commerce_tax_exemption\Entity\TaxExemption $tax_exemption
   *   The tax exemption in question.
   *
   * @return \Drupal\Core\TypedData\Type\DateTimeInterface|null
   *   DateTimeInterface definition.
   */
  public function getSignatureDate(TaxExemption $tax_exemption);

  /**
   * Gets the expiration date for a tax exemption.
   *
   * @param \Drupal\commerce_tax_exemption\Entity\TaxExemption $tax_exemption
   *   The tax exemption in question.
   *
   * @return \Drupal\Core\TypedData\Type\DateTimeInterface|null
   *   DateTimeInterface definition.
   */
  public function getExpirationDate(TaxExemption $tax_exemption);

  /**
   * Gets a URL to download / view the exemption certificate.
   *
   * @param \Drupal\commerce_tax_exemption\Entity\TaxExemption $tax_exemption
   *   The tax exemption in question.
   *
   * @return \Drupal\Core\Url
   *   Renderable URL object.
   */
  public function getCertificateUrl(TaxExemption $tax_exemption);

  /**
   * Gets the Exemption Reason for display.
   *
   * @param \Drupal\commerce_tax_exemption\Entity\TaxExemption $tax_exemption
   *   The tax exemption in question.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Renderable text string.
   */
  public function getReason(TaxExemption $tax_exemption);

  /**
   * Gets the validity of a TaxExemption entity.
   *
   * @param \Drupal\commerce_tax_exemption\Entity\TaxExemption $tax_exemption
   *   The tax exemption in question.
   * @param bool $require_approval
   *   Require approval for validations, not just expiration comparisons.
   *
   * @return bool
   *   TRUE if valid, otherwise FALSE.
   */
  public function isValid(TaxExemption $tax_exemption, $require_approval = TRUE);

  /**
   * Gets the label for the jurisdictions an exemption is valid for.
   *
   * @param \Drupal\commerce_tax_exemption\Entity\TaxExemption $tax_exemption
   *   The tax exemption in question.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]
   *   Renderable text string.
   */
  public function getJurisdictionLabels(TaxExemption $tax_exemption);

  /**
   * Resolves tax exemptions that apply to an OrderItem.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   *   The commerce order item.
   * @param \Drupal\profile\Entity\ProfileInterface $profile
   *   The address profile entity.
   * @param bool $require_approval
   *   If the exemption plugin supports approval,
   *   this should allow exemptions to be applied that have not be approved.
   *
   * @return \Drupal\commerce_tax_exemption\Entity\TaxExemption[]
   *   An array of tax exemptions that apply to the order item.
   */
  public function resolve(OrderItemInterface $order_item, ProfileInterface $profile, bool $require_approval = TRUE);

  /**
   * Gets the taxable type of an order item.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   *   The order item to retrieve the taxable type for.
   *
   * @return string
   *   A string containing the taxable type.
   */
  public function getOrderItemTaxableType(OrderItemInterface $order_item);

  /**
   * Gets the Order Item Type entity.
   *
   * @param \Drupal\commerce_order\Entity\OrderItem $order_item
   *   The order item to get the bundle for.
   *
   * @return \Drupal\commerce_order\Entity\OrderItemType
   *   Returns the bundle type entity.
   */
  public function getOrderItemBundle(OrderItem $order_item);

}
