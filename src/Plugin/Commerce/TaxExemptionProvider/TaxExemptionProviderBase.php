<?php

namespace Drupal\commerce_tax_exemption\Plugin\Commerce\TaxExemptionProvider;

use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_tax_exemption\Entity\TaxExemption;
use Drupal\commerce_tax_exemption\TaxExemptionProviderConfig;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for commerce_tax_exemption_provider plugins.
 */
abstract class TaxExemptionProviderBase extends PluginBase implements TaxExemptionProviderInterface, ContainerFactoryPluginInterface {

  use MessengerTrait;

  /**
   * Drupal Time Service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * Module handler interface.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Drupal Entity Type Manger Interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * This plugin configuration.
   *
   * @var array|null
   */
  protected $pluginConfig;

  /**
   * Constructs the Tax Exemption Provider Base object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Drupal Module Handler.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Drupal Entity Type Manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Drupal Configuration Factory.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, TimeInterface $time, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->time = $time;
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->pluginConfig = $config_factory->get(TaxExemptionProviderConfig::CONFIGURATION_KEY . $plugin_id)->get('configuration');

    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('datetime.time'),
      $container->get('module_handler'),
      $container->get('entity_type.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function supportsConfiguration() {
    if (isset($this->pluginDefinition['supports_configuration']) && $this->pluginDefinition['supports_configuration']) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function supportsApproval() {
    if (isset($this->pluginDefinition['supports_approval']) && $this->pluginDefinition['supports_approval']) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function save(TaxExemption $tax_exemption) {
    return $tax_exemption;
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // Add the submit button if the provider supports configuration.
    if ($this->supportsConfiguration()) {
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save Configuration'),
      ];
    }
    else {
      $form['config_error'] = [
        '#markup' => $this->t('This tax exemption does not support configuration.'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable(TaxExemptionProviderConfig::CONFIGURATION_KEY . $this->getPluginId());

    $plugin_config = [];

    foreach ($form_state->getValues() as $key => $value) {
      if (array_key_exists($key, $this->defaultConfiguration())) {
        $plugin_config[$key] = $value;
      }
    }

    $config->set('configuration', $plugin_config);

    $config->save();

    $this->messenger()->addMessage($this->t('Configuration saved for @provider_label', [
      '@provider_label' => $this->label(),
    ]));

    // Send the user back to the plugin admin page.
    $form_state->setRedirect('entity.commerce_tax_exemption.admin');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = NestedArray::mergeDeep($this->defaultConfiguration(), $configuration, $this->pluginConfig ? $this->pluginConfig : []);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label' => $this->pluginDefinition['label'],
    ];
  }

  /**
   * {@inheritdoc}
   *
   * You should override this method if your provider plugin supports approval.
   */
  public function isApproved(TaxExemption $tax_exemption) {
    if (!$this->supportsApproval()) {
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setAsApproved(TaxExemption $exemption) {
    if (!$this->supportsApproval()) {
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setAsUnapproved(TaxExemption $tax_exemption) {
    if (!$this->supportsApproval()) {
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getJurisdictionLabels(TaxExemption $tax_exemption) {
    $labels = [];
    if (!$tax_exemption->get('jurisdictions')->isEmpty()) {
      $profiles = $tax_exemption->get('jurisdictions')->referencedEntities();

      foreach ($profiles as $profile) {
        $labels[] = $this->t('@label', [
          '@label' => $profile->label(),
        ]);
      }
    }

    return $labels;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderItemTaxableType(OrderItemInterface $order_item) {
    $bundle = $this->getOrderItemBundle($order_item);

    if ($bundle->getThirdPartySettings('commerce_tax') && $taxable_type = $bundle->getThirdPartySetting('commerce_tax', 'taxable_type')) {
      return $taxable_type;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderItemBundle(OrderItem $order_item) {
    /** @var \Drupal\commerce_order\Entity\OrderItemType $bundle */
    $bundle = $this->entityTypeManager->getStorage('commerce_order_item_type')->load($order_item->bundle());
    return $bundle;
  }

}
