<?php

namespace Drupal\commerce_tax_exemption\Plugin\Commerce\EntityTrait;

use Drupal\commerce\Plugin\Commerce\EntityTrait\EntityTraitBase;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the Tax Exempt Order Item trait.
 *
 * @CommerceEntityTrait(
 *   id = "order_item_tax_exemptions",
 *   label = @Translation("Tax Exemptions"),
 *   entity_types = {"commerce_order_item"}
 * )
 */
class TaxExemptOrderItem extends EntityTraitBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = [];

    $fields['tax_exemptions'] = BundleFieldDefinition::create('entity_reference')
      ->setLabel('Tax Exemptions')
      ->setRequired(FALSE)
      ->setDescription($this->t('Tax exemptions that apply to this order item.'))
      ->setSetting('target_type', 'commerce_tax_exemption')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'weight' => 13,
      ])
      ->setCardinality(-1);

    return $fields;
  }

}
