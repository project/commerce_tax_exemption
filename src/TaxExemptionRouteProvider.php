<?php

namespace Drupal\commerce_tax_exemption;

use Drupal\commerce_tax_exemption\Access\TaxExemptionAccessCheck;
use Drupal\commerce_tax_exemption\Controller\TaxExemptionAdminController;
use Drupal\commerce_tax_exemption\Controller\TaxExemptionController;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\entity\Routing\DefaultHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Route provider for entity types using non-entity bundles.
 *
 * A lot of this code was taken and modified from drupal/entity_admin_handlers
 * created by joachim (https://www.drupal.org/project/entity_admin_handlers).
 *
 * This has been modified to be plugin bundle provider specific.
 */
class TaxExemptionRouteProvider extends DefaultHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    $entity_type_id = $entity_type->id();

    // Get the Admin base route for plugin configuration and field ui settings.
    if ($admin_base_route = $this->getAdminBaseRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.admin", $admin_base_route);
    }

    // Get the plugin configuration route.
    if ($plugin_configuration_route = $this->getPluginConfigurationRoute($entity_type)) {
      $collection->add("entity.{$entity_type_id}.plugin_configuration", $plugin_configuration_route);
    }

    // Add the User's entity creation route.
    if ($user_add_page = $this->getUserAddPage($entity_type)) {
      $collection->add("entity.{$entity_type_id}.user_add_page", $user_add_page);
    }

    // Add the User's entity creation form route.
    if ($user_add_form = $this->getUserAddForm($entity_type)) {
      $collection->add("entity.{$entity_type_id}.user_add_form", $user_add_form);
    }

    return $collection;
  }

  /**
   * Builds the Entity Add Form in the *.user form mode.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route
   *   The complete route.
   */
  protected function getUserAddForm(EntityTypeInterface $entity_type) {
    $route = new Route($entity_type->getLinkTemplate('user-add-form'));

    $route->setDefault('_title', 'Add tax exemption');
    $route->setDefault('entity_type_id', $entity_type->id());
    $route->setDefault('_entity_form', 'commerce_tax_exemption.user');
    $route->setRequirement('_custom_access', TaxExemptionAccessCheck::class . '::checkAccess');
    $route->setOption('parameters', [
      'user' => [
        'type' => 'entity:user',
      ],
    ]);

    return $route;
  }

  /**
   * Gets the User add page route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route
   *   The complete route.
   */
  protected function getUserAddPage(EntityTypeInterface $entity_type) {
    $route = new Route($entity_type->getLinkTemplate('user-add-page'));

    $route->setDefault('_controller', TaxExemptionController::class . '::userAddPage');
    $route->setDefault('_title', 'Add tax exemption');
    $route->setDefault('entity_type_id', $entity_type->id());
    $route->setRequirement('_custom_access', TaxExemptionAccessCheck::class . '::checkAccess');
    $route->setOption('parameters', [
      'user' => [
        'type' => 'entity:user',
      ],
    ]);

    return $route;
  }

  /**
   * Gets the admin base route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getAdminBaseRoute(EntityTypeInterface $entity_type) {
    $admin_permission = $entity_type->getAdminPermission();

    $route = new Route($entity_type->getLinkTemplate('plugin-configuration'));
    $route->setDefault('_controller', TaxExemptionAdminController::class . '::getAdminPage');
    $route->setDefault('_title', '@entity-label plugins');
    $route->setDefault('_title_arguments', [
      '@entity-label' => $entity_type->getLabel(),
    ]);
    $route->setDefault('entity_type_id', $entity_type->id());
    $route->setRequirement('_permission', $admin_permission);

    return $route;
  }

  /**
   * Gets the field UI base route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getPluginConfigurationRoute(EntityTypeInterface $entity_type) {
    $admin_permission = $entity_type->getAdminPermission();

    // Append a path component for the bundle parameter. The parameter must be
    // '{bundle}', as this is what
    // Drupal\field_ui\Routing\RouteSubscriber::alterRoutes() expects.
    $route = new Route($entity_type->getLinkTemplate('plugin-configuration') . '/{bundle}');

    $route->setDefault('_controller', TaxExemptionAdminController::class . '::getPluginConfigurationPage');
    $route->setDefault('_title', '@entity-label configuration');
    $route->setDefault('_title_arguments', [
      '@entity-label' => $entity_type->getLabel(),
    ]);
    $route->setDefault('entity_type_id', $entity_type->id());
    $route->setRequirement('_permission', $admin_permission);

    return $route;
  }

}
