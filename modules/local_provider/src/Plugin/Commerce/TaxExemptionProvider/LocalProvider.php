<?php

namespace Drupal\commerce_tax_exemption_local_provider\Plugin\Commerce\TaxExemptionProvider;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_tax_exemption\Entity\TaxExemption;
use Drupal\commerce_tax_exemption\Plugin\Commerce\TaxExemptionProvider\TaxExemptionProviderBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\entity\BundleFieldDefinition;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Plugin implementation of the commerce_tax_exemption_provider.
 *
 * @CommerceTaxExemptionProvider(
 *   id = "local_provider",
 *   label = @Translation("Local Provider"),
 *   description = @Translation("Base local exemption provider."),
 *   supports_approval = TRUE,
 *   supports_configuration = TRUE,
 * )
 */
class LocalProvider extends TaxExemptionProviderBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function resolve(OrderItemInterface $order_item, ProfileInterface $profile, bool $require_approval = TRUE) {
    return TaxExemption::loadMultiple($this->query($order_item, $profile, $require_approval)->execute());
  }

  /**
   * Generates an Entity Query for the resolver.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   *   The order item in question.
   * @param \Drupal\profile\Entity\ProfileInterface $profile
   *   The profiles to resolve exemptions for.
   * @param bool $require_approval
   *   If Approval is required for queried exemptions, set to TRUE.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   Returns a built up query.
   */
  protected function query(OrderItemInterface $order_item, ProfileInterface $profile, bool $require_approval) {

    $query = $this->entityTypeManager->getStorage('commerce_tax_exemption')->getQuery();

    // Only query for profiles owned by the order owner.
    $query->condition('uid', $profile->getOwnerId());

    // Query for the profile that matches the order's original taxable profile.
    $query->condition('jurisdictions', $profile->id());

    // Query for non-expired certificates only.
    $query->condition('expiration_date', $this->time->getCurrentTime(), '>=');

    // Set an approval condition on the query.
    if ($require_approval) {
      $query->condition('approved', TRUE);
    }

    $taxable_type = $this->getOrderItemTaxableType($order_item);

    $query->condition('taxable_types', $taxable_type);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function getSignatureDate(TaxExemption $tax_exemption) {
    if (!$tax_exemption->get('signed_date')->isEmpty()) {
      return DrupalDateTime::createFromTimestamp($tax_exemption->get('signed_date')->first()->getString());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getExpirationDate(TaxExemption $tax_exemption) {
    if (!$tax_exemption->get('expiration_date')->isEmpty()) {
      return DrupalDateTime::createFromTimestamp($tax_exemption->get('expiration_date')->first()->getString());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCertificateUrl(TaxExemption $tax_exemption) {
    if (!$tax_exemption->get('certificate')->isEmpty()) {
      /** @var \Drupal\file\Entity\File $file */
      $file = $tax_exemption->get('certificate')->first()->get('entity')->getTarget()->getValue();

      return Url::fromUri($file->createFileUrl(FALSE));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getReason(TaxExemption $tax_exemption) {
    if (!$tax_exemption->get('reason')->isEmpty()) {
      return $this->t('@reason', [
        '@reason' => $tax_exemption->get('reason')->first()->getString(),
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isValid(TaxExemption $tax_exemption, $require_approval = TRUE) {
    // Make sure the exemption entity is enabled.
    if (!$tax_exemption->isEnabled()) {
      return FALSE;
    }

    // Check for approval.
    if ($require_approval && !$tax_exemption->isApproved()) {
      return FALSE;
    }

    // Check for expiration.
    if ($this->getExpirationDate($tax_exemption)->getTimestamp() <= $this->time->getCurrentTime()) {
      return FALSE;
    }

    // Make sure there is at least one jurisdiction.
    if ($tax_exemption->get('jurisdictions')->isEmpty()) {
      return FALSE;
    }

    // It should be valid at this point.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function isApproved(TaxExemption $tax_exemption) {
    if (!$tax_exemption->get('approved')->isEmpty()) {
      return boolval($tax_exemption->get('approved')->first()->getString());
    }
    else {
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setAsApproved(TaxExemption $tax_exemption) {
    return boolval($tax_exemption->set('approved', TRUE)->save());
  }

  /**
   * {@inheritdoc}
   */
  public function setAsUnapproved(TaxExemption $tax_exemption) {
    return boolval($tax_exemption->set('approved', FALSE)->save());
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'default_expiration' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    // Default expiration.
    $form['default_expiration'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default expiration on certificates'),
      '#description' => $this->t('Uses <a href="@strtotime_url" target="_blank" title="PHP strtotime() documentation">PHP strtotime</a> function against the date the certificate is signed. Example: "+1 month", "next Thursday", 07/03/2023"', [
        '@strtotime_url' => 'https://www.php.net/manual/en/function.strtotime.php',
      ]),
      '#default_value' => $this->configuration['default_expiration'],
      '#required' => TRUE,
    ];

    return $form + parent::buildConfigurationForm($form, $form_state);
  }

  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $default_expiration = $form_state->getValue('default_expiration');
    if (!$default_expiration) {
      $form_state->setErrorByName('default_expiration', 'The default expiration date needs to be set.');
    }

    // Validate that the expiration string is valid and set to a time in the future.
    $now = $this->time->getCurrentTime();
    if ($this->createExpirationDate($now, $default_expiration) < $now) {
      $form_state->setErrorByName('default_expiration', 'The default expiration value must be valid and set to a time in the future.');
    }

    return parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * Generates an epoc timestamp for expiration from a start date.
   *
   * @param int $start_date
   *   The timestamp to start on.
   * @param string $expiration
   *   A valid strtotime() string.
   *
   * @return false|int
   *   Returns false if strtotime() string is invalid, otherwise the timestamp.
   */
  private function createExpirationDate(int $start_date, string $expiration) {
    if ($timestamp = strtotime($expiration, $start_date)) {
      return $timestamp;
    }
  }

  /**
   * {@inheritdoc}
   *
   * Adds pre-configured the expiration date if not set in the form.
   */
  public function save(TaxExemption $tax_exemption) {
    if ($this->configuration['default_expiration'] && $tax_exemption->get('expiration_date')->isEmpty() && $signature_date = $tax_exemption->getSignatureDate()) {
      // If the expiration date is not filled out, set the default.
      $new_expiration_date = $this->createExpirationDate($signature_date->format('U'), $this->configuration['default_expiration']);
      // Set the expiration date value.
      $tax_exemption->set('expiration_date', $new_expiration_date);
    }

    return parent::save($tax_exemption);
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {

    $fields = [];

    $fields['certificate'] = BundleFieldDefinition::create('file')
      ->setRevisionable(TRUE)
      ->setLabel($this->t('Exemption Certificate'))
      ->setDescription($this->t('The source document for exemption.'))
      ->setRequired(TRUE)
      ->setSettings([
        'uri_scheme' => 'private',
        'file_directory' => 'tax-exemptions/certificates',
        'file_extensions' => 'png jpg jpeg pdf',
      ])
      ->setCardinality(1)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['signed_date'] = BundleFieldDefinition::create('timestamp')
      ->setRevisionable(TRUE)
      ->setLabel($this->t('Signature Date'))
      ->setRequired(TRUE)
      ->setDescription($this->t('The date this document was signed.'))
      ->setCardinality(1)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['expiration_date'] = BundleFieldDefinition::create('timestamp')
      ->setLabel($this->t('Expiration Date'))
      ->setDescription($this->t('The timestamp in which this document expires.'))
      ->setRevisionable(TRUE)
      ->setRequired(TRUE)
      ->setCardinality(1)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['reason'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Reason for Exemption'))
      ->setDescription($this->t('The reason for the exemption certificates validity.'))
      ->setRevisionable(TRUE)
      ->setRequired(TRUE)
      ->setCardinality(1)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['jurisdictions'] = BundleFieldDefinition::create('entity_reference')
      ->setLabel($this->t('Jurisdiction(s)'))
      ->setDescription($this->t('Choose an address from your address book that this exemption relates to.'))
      ->setSetting('target_type', 'profile')
      ->setRevisionable(TRUE)
      ->setRequired(TRUE)
      ->setCardinality(-1)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['approved'] = BundleFieldDefinition::create('boolean')
      ->setLabel($this->t('Approved'))
      ->setDescription($this->t('The approval state of the tax exemption.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(FALSE)
      ->setRequired(FALSE)
      ->setCardinality(1)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
